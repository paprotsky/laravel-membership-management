<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::redirect('/home', '/members', 301);

Route::group(['middleware' => ['auth']], function () {
    Route::resource('languages', 'LanguageController');

    Route::resource('members', 'MemberController');

    Route::resource('interests', 'InterestController');

    Route::get('/settings', 'SettingsController@index');
});


