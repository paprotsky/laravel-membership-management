# Members Database

Members Database built with [Laravel 5.7](https://laravel.com/).

##### [Demo](http://members.romanpaprotsky.com/)

##### [Screencast](https://www.youtube.com/watch?v=LcK0p7l7GYE)

### Setup
- Clone the project.

- Go to the folder application using `cd` command on your terminal.
```sh
$ composer install
```
- Copy `.env.example` file to `.env` on the root folder.

- Open your `.env` file and change the database name `DB_DATABASE` to whatever you have, username `DB_USERNAME` and password `DB_PASSWORD` field correspond to your configuration.

```sh
$ php artisan key:generate
$ php artisan migrate
$ php artisan serve
```
- Go to [localhost:8000](http://localhost:8000)
