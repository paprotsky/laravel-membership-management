@extends('layouts.app')

@section('title', 'Create Member')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Edit Member <br>
                </div>
                <div class="card-body">
                    <form method="POST" action="/members/{{ $member->id }}">
                        @method('PATCH')
                        @csrf

                        <div class="row">
                            <div class="col-sm-3 text-center">
                                {{-- <img src="https://randomuser.me/api/portraits/men/{{ rand(1,90) }}.jpg" alt="avatar" class="avatar-lg"> --}}
                                <i class="fas fa-user-circle fa-8x"></i>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label>First Name *</label>
                                    <input type="text" class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" name="first_name" value="{{ $member->first_name }}" placeholder="First Name">
                                </div>

                                <div class="form-group">
                                    <label>Last Name *</label>
                                    <input type="text" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" name="last_name" value="{{ $member->last_name }}" placeholder="Larst Name">
                                </div>

                                <div class="form-group mt-5">
                                    <label>Id Number *</label>
                                    <input type="text" class="form-control {{ $errors->has('id_number') ? 'is-invalid' : '' }}" name="id_number" value="{{ $member->id_number }}" placeholder="Id Number">
                                </div>

                                <div class="form-group">
                                    <label>Email Address *</label>
                                    <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ $member->email }}" placeholder="Email Address">
                                </div>

                                <div class="form-group">
                                    <label>Mobile Number *</label>
                                    <input type="text" class="form-control {{ $errors->has('mobile') ? 'is-invalid' : '' }}" name="mobile" value="{{ $member->mobile }}" placeholder="Mobile Number">
                                </div>

                                <div class="form-group mt-5">
                                    <label>Interests *</label>
                                    <div>
                                        @if (count($interests) > 0)
                                            <select class="selectpicker" multiple name="interests[]" id="interests">
                                                @foreach ($interests as $interest)
                                                    <option value="{{ $interest->id }}">{{ $interest->name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <span class="text-black-50">Please, set languages in </span><a href="/settings">Settings</a>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Language *</label>
                                    <div>
                                        @if (count($languages) > 0)
                                            <select class="selectpicker" name="language_id" id="language_id" data-live-search="true" value="{{ $member->language_id }}">
                                                @foreach ($languages as $language)
                                                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <span class="text-black-50">Please, set languages in </span><a href="/settings">Settings</a>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Birth Date *</label>
                                    <div>
                                        <select class="selectpicker" id="day" data-width="80px" name="day">
                                            <option disabled selected>Day</option>
                                            @for ($i = 1; $i <= 31; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>

                                        <select class="selectpicker" id="month" data-width="120px" name="month">
                                            <option disabled selected>Month</option>
                                            @foreach ($months as $month)
                                                <option value="{{++$loop->index}}">{{ $month }}</option>
                                            @endforeach
                                        </select>

                                        <select class="selectpicker" id="year" data-width="80px" name="year">
                                            <option disabled selected>Year</option>
                                            @for ($i = $currentYear; $i >= $currentYear - 80; $i--)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>

                                @include('parts.errors')

                                <div class="form-group">
                                    <small class="text-muted">* All fields are required</small>
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
                           <button type="submit" class="btn btn-primary mx-3 my-4">Save changes</button>
                        </div>
                    </form>

                    <form method="POST" action="/members/{{ $member->id }}">
                        @method('DELETE')
                        @csrf

                        <div class="delete-btn">
                           <button type="submit" class="btn btn-danger mx-3">
                                <i class="fas fa-trash-alt mr-2"></i>
                                Delete
                            </button>
                        </div>
                    </form>

{{--                     <form>
                        <div class="delete-btn">
                           <button type="submit" class="btn btn-danger mx-3">Delete</button>
                        </div>
                    </form> --}}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        // Set Selectpickers
        var interests = @json($member->interests);
        var ids = interests.map((interests) => interests.id);

        $('#interests').selectpicker('val', ids);
        $('#language_id').selectpicker('val', {{ $member->language_id }});
        $('#day').selectpicker('val', 9);

        var birth_date = @json($member->birth_date);
        $('#day').selectpicker('val', parseInt(birth_date.substr(8, 2)));
        $('#month').selectpicker('val', parseInt(birth_date.substr(5, 2)));
        $('#year').selectpicker('val', parseInt(birth_date.substr(0, 4)));
    });
</script>

@endsection




