@extends('layouts.app')

@section('title', 'Member')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Member
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3 text-center">
                            {{-- <img src="https://randomuser.me/api/portraits/men/{{ rand(1,90) }}.jpg" alt="avatar" class="avatar-lg"> --}}
                            <i class="fas fa-user-circle fa-8x"></i>
                        </div>
                        <div class="col-sm-9">
                            <div class="btn-toolbar justify-content-between">
                                <div>
                                    <h5 class="card-title pt-2 text-uppercase">{{ $member->first_name }} {{ $member->last_name }}</h5>
                                </div>
                                <div>
                                    <a class="btn btn-primary" href="/members/{{ $member->id }}/edit" role="button"><i class="far fa-edit mr-2"></i>Edit</a>
                                </div>
                            </div>

                            <div class="card-text">
                                <p>
                                    <i class="far fa-id-card"></i>{{ $member->id_number }}
                                </p>
                                <span class="text-muted">
                                    <i class="far fa-envelope"></i>
                                    {{ $member->email }}
                                </span><br>
                                <span class="text-muted">
                                    <i class="fas fa-mobile-alt"></i>
                                    {{ $member->mobile }}
                                </span><br>
                                <span class="text-muted">
                                    <i class="fas fa-birthday-cake"></i>
                                    {{ Carbon\Carbon::parse($member->birth_date)->format('d/m/Y') }}
                                </span><br>
                                <span class="text-muted">
                                    <i class="fas fa-globe-africa"></i>
                                    {{ $member->language->name }}
                                </span><br>
                                <dl class="mt-4">
                                    <dt>Interests</dt>
                                    <dd class="text-muted">
                                        @foreach ($member->interests as $interest)
                                            {{ $interest->name }}@if ($loop->index < count($member->interests) - 1),@endif
                                        @endforeach
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



