@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Members</div>

                <div class="card-body">
                    @if (count($members) > 0)
                        <table class="table table-hover members-table">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Id Number</th>
                                    <th scope="col">Mobile Number</th>
                                    <th scope="col">Email Address</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($members as $member)
                                <tr>
                                    <td>{{ ++$loop->index }}</td>
                                    <td>
                                        {{-- <img src="https://randomuser.me/api/portraits/men/{{ rand(1,90) }}.jpg" alt="avatar" class="avatar"> --}}
                                        <i class="fas fa-user-circle fa-2x"></i>
                                    </td>
                                    <td>{{ $member->first_name }}</td>
                                    <td>{{ $member->last_name }}</td>
                                    <td>{{ $member->id_number }}</td>
                                    <td>{{ $member->mobile }}</td>
                                    <td>{{ $member->email }}</td>
                                    <td><a href="/members/{{ $member->id }}">View</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>

            <div class="text-right">
                <a class="btn btn-primary mx-3 my-3" href="/members/create" role="button"><i class="fas fa-user-plus mr-2"></i>Add New Member</a>
            </div>
        </div>
    </div>
</div>
@endsection


