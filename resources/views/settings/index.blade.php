@extends('layouts.app')

@section('content')
<div class="container">
    <div class="justify-content-center mb-4">
        <h4>Settings</h4>
        @include('parts.errors')
    </div>
    <div class="row">
        <div class="col-md-6">
            <h5>Languages</h5>

            <ul class="list-group">
                @foreach ($languages as $language)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{ $language->name }}

                        <form method="POST" action="/languages/{{ $language->id }}">
                            @method('DELETE')
                            @csrf

                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash-alt mr-2"></i>
                                Delete
                            </button>
                        </form>
                    </li>
                @endforeach
            </ul>

            <form method="POST" action="/languages">
                @csrf

                <div class="input-group my-3">
                    <input type="text" class="form-control" name="name" placeholder="New language">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-plus mr-2"></i>
                            Add Language
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <h5>Interests</h5>

            <ul class="list-group">
                @foreach ($interests as $interest)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{ $interest->name }}

                        <form method="POST" action="/interests/{{ $interest->id }}">
                            @method('DELETE')
                            @csrf

                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash-alt mr-2"></i>
                                Delete
                            </button>
                        </form>
                    </li>
                @endforeach
            </ul>

            <form method="POST" action="/interests">
                @csrf

                <div class="input-group my-3">
                    <input type="text" class="form-control" name="name" placeholder="New interest">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-plus mr-2"></i>
                            Add Interest
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
