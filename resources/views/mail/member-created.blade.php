@component('mail::message')
# Hi {{ $member->first_name }}!

Welcome to the Members Database App!

@component('mail::button', ['url' => 'https://google.com'])
Sign Up Now
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
