<?php

namespace App\Listeners;

use App\Events\MemberCreated;
use App\Mail\MemberCreatedMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendMemberCreatedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MemberCreated  $event
     * @return void
     */
    public function handle(MemberCreated $event)
    {
        Mail::to($event->member->email)->send(
            new MemberCreatedMail($event->member)
        );
    }
}
