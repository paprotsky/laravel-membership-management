<?php

namespace App\Http\Controllers;

use App\Interest;
use App\Language;
use App\Events\MemberCreated;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    private $months = [
        '1' => 'January',
        '2' => 'February',
        '3' => 'March',
        '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('members.index', [
            'members' => auth()->user()->members
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dt = Carbon::now();
        $currentYear = $dt->year;
        $months = $this->months;

        $interests = Interest::all();
        $languages = Language::all();

        return view('members.create', compact(
            'currentYear',
            'interests',
            'languages',
            'months'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newMember = $this->validateMember();

        $year = request()->year;
        $month = request()->month;
        $day = request()->day;
        $birth_date = Carbon::create($year, $month, $day);

        $newMember['birth_date'] = $birth_date->toDateString();
        $newMember['user_id'] = auth()->id();

        $member = Member::create($newMember);

        $member->interests()->attach($newMember['interests']);

        event(new MemberCreated($member));

        return redirect('/members');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        // $this->authorize('update', $member);

        $member = Member::with('interests')->with('language')->find($member->id);

        return view('members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $member = Member::with('interests')->find($member->id);

        $interests = Interest::all();
        $languages = Language::all();

        $dt = Carbon::now();
        $currentYear = $dt->year;
        $months = $this->months;

        return view('members.edit', compact(
            'currentYear',
            'interests',
            'languages',
            'member',
            'months'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $year = request()->year;
        $month = request()->month;
        $day = request()->day;
        $birth_date = Carbon::create($year, $month, $day);

        $member['birth_date'] = $birth_date->toDateString();

        $member->update($this->validateMember());

        $member->interests()->sync($request->interests);

        return redirect('/members/' . $member->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->interests()->detach();

        $member->delete();

        return redirect('/members');
    }

    protected function validateMember()
    {
        return request()->validate([
            'first_name' => ['required', 'min:3', 'max:100'],
            'last_name' => ['required', 'min:3', 'max:100'],
            'id_number' => ['required', 'digits:11'],
            'email' => ['required', 'email'],
            'mobile' => ['required', 'min:10', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'interests' => ['required'],
            'language_id' => ['required', 'numeric'],
            'day' => ['required'],
            'month' => ['required'],
            'year' => ['required']
        ]);
    }
}
