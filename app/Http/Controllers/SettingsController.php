<?php

namespace App\Http\Controllers;

use App\Interest;
use App\Language;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interests = Interest::all();
        $languages = Language::all();

        return view('settings.index', compact(
            'interests',
            'languages'
        ));
    }
}

